from flask.ext.migrate import Migrate, MigrateCommand
from app import app, db
from flask.ext.script import Manager

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

manager.run()