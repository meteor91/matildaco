import getpass

from app import db
from app.models import User, ROLE_ADMIN
from sqlalchemy.exc import IntegrityError

username = "admin"

user = db.session.query(User).filter(User.username == username).first()
exist = False
if None != user:
    exist = True

if exist:
    print "Admin user currently exist, type 'yes' to reset your password: "
    answer = raw_input("(yes/no): ")
    if answer == "no":
        exit()
else:
    print "Greeting! You need to define admin user password, to access administrative tools."
password = getpass.getpass('Password: ')

while True:
    email = raw_input("Email: ")
    try:
        if exist:
            user.set_password(password)
            user.email = email
            user.role = ROLE_ADMIN
        else:
            user = User(username, password, email, ROLE_ADMIN)
            db.session.add(user)
        db.session.commit()
    except IntegrityError as e:
        print 'Someone already created account with same email. Pls try another one:'
        continue
    finally:
        db.session.rollback()
    break

if exist:
    print "admin user updated, use 'admin' username and your password to log in as admin!"
else:
    print "admin user created, use 'admin' username and your password to log in as admin!"
