"""empty message

Revision ID: 2c57be70de13
Revises: 327987f5624
Create Date: 2015-03-21 19:56:32.210407

"""

# revision identifiers, used by Alembic.
revision = '2c57be70de13'
down_revision = '327987f5624'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('post', 'message',
               type_=sa.Text())
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('post', 'message',
               existing_type=sa.VARCHAR(length=255))
    ### end Alembic commands ###
