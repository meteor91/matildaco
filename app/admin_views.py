from app import login_manager, db, THREADS_PER_PAGE, POSTS_PER_PAGE
from app.models import Thread
from flask import request, render_template, Blueprint, url_for, redirect
from flask_login import current_user
from flask_wtf import Form
from models import User, Thread, Post, Vote
import flask
from sqlalchemy import and_, func
from sqlalchemy.sql import text
from functools import wraps
from flask.ext.login import current_user, login_required

import model_manager

admin = Blueprint('admin', __name__, template_folder='templates')


def admin_only(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_user.is_admin:
            return func(*args, **kwargs)
        else:
            return redirect(url_for('index'))
    return decorated_view


@admin.route('/thread', methods=['GET'])
@login_required
@admin_only
def thread():
    page = int(request.args.get('page') if request.args.get('page') else 1)
    threads = model_manager.thread_list(page, THREADS_PER_PAGE)
    return render_template('thread/admin.html', threads=threads)


@admin.route('/thread/<int:id>/delete', methods=['POST'])
@login_required
@admin_only
def thread_delete(id):
    form = Form()
    if form.validate():
        db.engine.execute(text(model_manager.update_user_archive_posts), thread_id=id)
        db.engine.execute(text(model_manager.update_user_archive_reputation), thread_id=id)
        db.session.query(Thread).filter(Thread.id == id).delete()
        db.session.commit()
    return redirect(url_for('admin.thread'))