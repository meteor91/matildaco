from . import db
from sqlalchemy import func, and_, desc
from sqlalchemy.orm import aliased
from flask_sqlalchemy import Pagination

from models import Post, Thread, Vote, User


def update_password(user, new_password):
    user = db.session.query(User).filter(User.id == user.id).first()
    user.set_password(new_password)
    db.session.commit()


def thread_list(page, per_page):
    subquery_post_count = (db.session.query(
        Post.thread_id,
        func.count(Post.thread_id).label('posts_count'),
        func.max(Post.id).label('post_id')
    ).group_by(Post.thread_id)).subquery()

    author = aliased(User)
    query = db.session.query(Thread, 'posts_count', Post.created_at, author) \
        .join(subquery_post_count, and_(Thread.id == subquery_post_count.c.thread_id)) \
        .join(Post, and_(Post.thread_id == Thread.id, Post.id == subquery_post_count.c.post_id)) \
        .join(author, and_(Post.user_id == author.id)) \
        .order_by(desc(Post.created_at))
    return paginate(query, page, per_page, db.session.query(Thread))


def posts_list(thread_id, page, per_page):
    post_rate = db.session.query(Vote.post_id, func.sum(Vote.vote).label('rate')) \
        .group_by(Vote.post_id).subquery()
    query = db.session.query(Post, 'rate') \
        .outerjoin(post_rate, and_(post_rate.c.post_id == Post.id)) \
        .filter(Post.thread_id == thread_id).order_by(Post.id)
    count_query = db.session.query(Post.id).filter(Post.thread_id == thread_id)
    return paginate(query, page, per_page, count_query)


def post_reputation(post_id):
    return db.session.query(func.sum(Vote.vote)).filter(Vote.post_id == post_id).scalar()


def vote(user, post_id, v):
    user_id = user.get_id()
    vote_own = db.session.query(Post.id).filter(Post.user_id == user_id, Post.id == post_id).first()
    if None != vote_own:
        print ('vote_own')
        return 'vote_own', None

    v = 1 if v > 0 else -1
    vote = db.session.query(Vote).filter(Vote.user_id == user_id, Vote.post_id == post_id).first()
    if None == vote:
        vote = Vote(user_id, post_id, v)
        db.session.add(vote)
    elif vote.vote == v:
        vote.vote = 0
    else:
        vote.vote = v
    db.session.commit()
    rate = str(post_reputation(vote.post_id))
    return 'success', rate


def paginate(query, page, per_page, total_query, error_out=True):
    if error_out and page < 1:
        return None
    items = query.limit(per_page).offset((page - 1) * per_page).all()
    if not items and page != 1 and error_out:
        return None

    # No need to count if we're on the first page and there are fewer
    # items than we expected.
    if page == 1 and len(items) < per_page:
        total = len(items)
    else:
        total = total_query.count()

    return Pagination(query, page, per_page, total, items)



update_user_archive_posts = """
update "user"
set (archived_posts) = (archived_posts+t.c)
from (
    select u.user_id ,count(p) as c
	from "user" as u
	join post as p on p.user_id =u.user_id and p.thread_id = :thread_id
	group by u.user_id

) as t
where t.user_id = "user".user_id;
"""
update_user_archive_reputation = """
update "user"
set (archived_reputation) = (archived_reputation+t.r)
from (
	select u.user_id, sum(v.vote) as r
	from "user" as u
	join post as p on p.user_id =u.user_id and p.thread_id = :thread_id
	join vote as v on v.post_id = p.post_id
	group by u.user_id
) as t
where t.user_id = "user".user_id;
"""