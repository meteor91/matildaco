from flask.ext.login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from . import db
import datetime
from sqlalchemy import func, and_, select
from sqlalchemy.orm import object_session
from sqlalchemy.ext.hybrid import hybrid_property

ROLE_USER = 0
ROLE_ADMIN = 1


class User(db.Model, UserMixin):
    __tablename__ = "user"
    id = db.Column('user_id', db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, index=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(50), unique=True, index=True, nullable=False)
    registered_on = db.Column(db.DateTime)
    social_id = db.Column(db.String(64), unique=True)
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    about = db.Column(db.String(255))
    archived_posts = db.Column(db.Integer, default=0, nullable=False)
    archived_threads = db.Column(db.Integer, default=0, nullable=False)
    archived_reputation = db.Column(db.Integer, default=0, nullable=False)

    @hybrid_property
    def posts_count(self):
        return object_session(self).query(Post).filter(Post.user == self).count()

    @posts_count.expression
    def posts_count(cls):
        return select([func.count(Post.id)]).where(Post.user_id == cls.id).label('posts_count')

    @hybrid_property
    def threads_count(self):
        return object_session(self).query(Thread).filter(Thread.user == self).count()

    @threads_count.expression
    def threads_count(cls):
        return select([func.count(Thread.id)]).where(Thread.user_id == cls.id).label('threads_count')

    @hybrid_property
    def rating(self):
        rating = db.session.query(func.sum(Vote.vote)) \
            .join(Post, and_(Post.id == Vote.post_id, Post.user_id == self.id)) \
            .scalar()
        return 0 if not rating else rating

    def __init__(self, username, password, email, role=ROLE_USER, social_id=None):
        self.username = username
        self.set_password(password)
        self.email = email
        self.role = role
        self.social_id = social_id
        self.registered_on = datetime.datetime.utcnow()

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @property
    def is_admin(self):
        return self.role == ROLE_ADMIN


class Thread(db.Model):
    id = db.Column('thread_id', db.Integer, primary_key=True)
    title = db.Column(db.String(127), unique=True, nullable=False)
    created_at = db.Column(db.DateTime)

    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    user = db.relationship("User")

    def __init__(self, title, user):
        self.title = title
        self.created_at = datetime.datetime.now()
        self.user = user


class Post(db.Model):
    id = db.Column('post_id', db.Integer, primary_key=True)
    message = db.Column(db.Text(), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    user = db.relationship("User", backref=db.backref('posts', lazy='dynamic'))

    thread_id = db.Column(db.Integer, db.ForeignKey('thread.thread_id'), nullable=False)
    thread = db.relationship("Thread", cascade="delete")

    def __init__(self, message, user, thread):
        self.message = message
        self.created_at = datetime.datetime.now()
        self.thread = thread
        self.user = user


class Vote(db.Model):
    id = db.Column('vote_id', db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey('post.post_id', ondelete="CASCADE"), nullable=False)
    vote = db.Column(db.SmallInteger, nullable=False)

    def __init__(self, user_id, post_id, vote):
        self.post_id = post_id
        self.user_id = user_id
        self.vote = vote

    @staticmethod
    def post_reputation(post_id):
        return db.session.query(func.sum(Vote.vote)).filter(Vote.post_id == post_id).scalar()


db.Index('vote_index', Vote.user_id, Vote.post_id, unique=True)