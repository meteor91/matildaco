from flask_wtf import Form
from wtforms import StringField, PasswordField, TextAreaField, validators, BooleanField, IntegerField
from wtforms.fields.html5 import EmailField
from flask.ext.babel import lazy_gettext as _


class LoginForm(Form):
    username = StringField(_(u'User name'), [validators.DataRequired()])
    password = PasswordField(_(u'Password'), [validators.DataRequired()])
    remember_me = BooleanField(_(u'Remember me'), [validators.Optional()])


class RegisterForm(Form):
    username = StringField(_(u'User name'), [validators.DataRequired()])
    email = EmailField(_(u'email'), [validators.Email(), validators.Length(min=6, max=35)])
    accept_tos = BooleanField(_(u'Terms and conventions'), [validators.DataRequired()])
    password = PasswordField(_(u'Password'), [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField(_(u'Repeat Password'))


class UserForm(Form):
    username = StringField(_(u'User name'), [validators.DataRequired()])
    email = EmailField(_(u'email'), [validators.Email(), validators.Length(min=6, max=35)])
    about = TextAreaField(_(u'About'), [validators.Optional()])


class ChangePassForm(Form):
    password = PasswordField(_(u'Password'), [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField(_(u'Repeat Password'))


class ThreadForm(Form):
    title = StringField(_(u'Title'), [validators.DataRequired()])
    message = TextAreaField(_(u'Message'), [validators.DataRequired()])


class PostForm(Form):
    message = TextAreaField(_(u'Message'), [validators.DataRequired()])


class VoteForm(Form):
    post_id = IntegerField('post_id', [validators.DataRequired()])
    vote = IntegerField('vote', [validators.DataRequired()])