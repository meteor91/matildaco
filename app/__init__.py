from flask import Flask
from flask.ext.login import LoginManager
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.babel import Babel
from flask_wtf.csrf import CsrfProtect
import os

SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

POSTS_PER_PAGE = 10
THREADS_PER_PAGE = 10

app = Flask(__name__)
app.config['OAUTH_CREDENTIALS'] = {
    'facebook': {
        'id': os.environ['facebook_id'],
        'secret': os.environ['facebook_secret']
    }
}
app.config['threads_per_page'] = THREADS_PER_PAGE
app.config.from_object(__name__)
app.secret_key = "yeah, not actually a secret"

app.config.from_object('config')
app.config['BABEL_DEFAULT_LOCALE'] = 'ru'
babel = Babel(app, default_locale='ru')

db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"
login_manager.setup_app(app)

csrf = CsrfProtect()
csrf.init_app(app)

from app import views, models
from admin_views import admin
app.register_blueprint(admin, url_prefix='/admin')
