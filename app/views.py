from app.helpers import flash_errors
from flask import render_template, request, url_for, redirect, flash, abort
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import app, login_manager, db, THREADS_PER_PAGE, POSTS_PER_PAGE
from models import User, Thread, Post, Vote
from forms import LoginForm, ThreadForm, PostForm, RegisterForm, VoteForm, UserForm, ChangePassForm
from oauth import OAuthSignIn
from flask import jsonify
from flask.ext.babel import gettext as _
from sqlalchemy.exc import IntegrityError

import model_manager as model_manager

@login_manager.user_loader
def load_user(id):
    return User.query.get(id)

@app.context_processor
def utility_processor():
    def thread_last_page(posts_count):
        return posts_count / THREADS_PER_PAGE + 1
    return dict(thread_last_page=thread_last_page)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/login', methods=['GET'])
def login():
    return render_template('login.html', form=LoginForm())


@app.route('/login', methods=['POST'])
def login_check():
    print 'here we go'
    form = LoginForm(request.form)
    if form.validate():
        username = form.username.data
        password = form.password.data
        remember_me = form.remember_me.data
        registered_user = User.query.filter_by(username=username).first()

        if registered_user is None or not registered_user.check_password(password) or ('social' == password):
            return redirect(url_for('login'))

        login_user(registered_user, remember=remember_me)
        return redirect(url_for('index'))
    flash_errors(form)
    return redirect(url_for('login'))

@app.route('/register', methods=['GET'])
def register():
    form = RegisterForm()
    return render_template('register.html', form=form)


@app.route('/register', methods=['POST'])
def register_do():
    form = RegisterForm(request.form)
    if form.validate():
        user = User(username=form.username.data,
                    password=form.password.data,
                    email=form.email.data)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('index'))
    flash_errors(form)
    return redirect('register')


@app.route("/logout")
@login_required
def logout():
    logout_user();
    return redirect(url_for('index'))


@app.route('/authorize/<provider>')
def oauth_authorize(provider):
    if not current_user.is_anonymous():
        return redirect(url_for('index'))
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()


@app.route('/callback/<provider>')
def oauth_callback(provider):
    if not current_user.is_anonymous():
        return redirect(url_for('index'))
    oauth = OAuthSignIn.get_provider(provider)
    social_id, username, email = oauth.callback()
    if social_id is None:
        flash('Authentication failed.')
        return redirect(url_for('index'))
    user = User.query.filter_by(social_id=social_id).first()
    if not user:
        user = User(social_id=social_id, username=username, password='social', email=email)
        db.session.add(user)
        db.session.commit()
    login_user(user, True)
    return redirect(url_for('index'))


@app.route('/')
def index():
    page = int(request.args.get('page') if request.args.get('page') else 1)
    threads = model_manager.thread_list(page, THREADS_PER_PAGE)
    return render_template('index.html', threads=threads)


@app.route('/thread/<int:id>', methods=['GET'])
@app.route('/thread/<int:id>/page/<int:page>', methods=['GET'])
def thread_show(id, page=1):
    thread = Thread.query.get(id)
    if not thread:
        abort(404)
    posts = model_manager.posts_list(thread.id, page, POSTS_PER_PAGE)
    if not posts:
        abort(404)
    return render_template('thread/show.html', thread=thread, posts=posts, form=PostForm())


@app.route('/thread/<int:id>', methods=['POST'])
@app.route('/thread/<int:id>/page/<int:page>', methods=['POST'])
@login_required
def post_store(id, page=1):
    form = PostForm(request.form)
    if form.validate():
        thread = Thread.query.get(id)
        message = form.message.data
        post = Post(message, current_user, thread)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('thread_show', id=id, page=page))

    flash_errors(form)
    return redirect(url_for('thread_show', id=id, page=page))


@app.route('/thread/create', methods=['GET'])
@login_required
def thread_create():
    form = ThreadForm()
    return render_template('thread/create.html', form=form)


@app.route('/thread/create', methods=['POST'])
@login_required
def thread_store():
    form = ThreadForm(request.form)
    if form.validate():
        title = form.title.data
        message = form.message.data
        thread = Thread(title, current_user)
        try:
            db.session.add(thread)
            db.session.commit()
        except IntegrityError:
            flash(_('Theme with same name already exists'))
            db.session.rollback()
            return redirect(url_for('thread_create'))

        db.session.add(Post(message, current_user, thread))
        db.session.commit()
        return redirect(url_for('thread_show', id=thread.id))
    flash_errors(form)
    return redirect(url_for('thread_create'))


@app.route('/profile/<int:id>', methods=['GET'])
def user_get(id):
    try:
        user, posts_count, threads_count = db.session.query(User, User.posts_count, User.threads_count).filter(User.id == id).first()
    except TypeError:
        abort(404)

    posts_count = posts_count + user.archived_posts
    threads_count = threads_count + user.archived_threads
    reputation = user.rating + user.archived_reputation

    if current_user.is_anonymous():
        own = False
    elif id == current_user.id:
        own = True
    else:
        own = False
    return render_template('user/show.html',
                           reputation=reputation,
                           user=user,
                           posts_count=posts_count,
                           threads_count=threads_count,
                           own=own)


@app.route('/profile/edit', methods=['GET'])
@login_required
def user_edit():
    form = UserForm(obj=current_user)
    return render_template('user/edit.html', form=form)


@app.route('/profile/edit', methods=['POST'])
@login_required
def user_update():
    form = UserForm(request.form)

    if form.validate():
        user = User.query.get(current_user.id)
        form.populate_obj(user)
        db.session.commit()
        return redirect(url_for('user_get', id=current_user.id))

    flash_errors(form)
    return redirect(url_for('user_edit'))

@app.route('/profile/pass/edit', methods=['GET'])
@login_required
def user_pass_edit():
    form = ChangePassForm()
    return render_template('user/edit_pass.html', form=form)


@app.route('/profile/pass/edit', methods=['POST'])
@login_required
def user_pass_update():
    form = ChangePassForm(request.form)

    if form.validate():
        model_manager.update_password(current_user, form.password.data)
        return redirect(url_for('user_get', id=current_user.id))

    flash_errors(form)
    return redirect(url_for('user_pass_edit'))


@app.route('/vote', methods=['POST'])
def vote():
    if not current_user.is_authenticated():
        return jsonify(code='unknown_user')

    form = VoteForm(request.form)
    if form.validate():
        code, new_rate = model_manager.vote(current_user, form.post_id.data, form.vote.data)
        return jsonify(code=code, rate=new_rate)

    return jsonify(code='invalid_form')


@app.route('/terminal', methods=['GET'])
def terminal():
    return render_template('terminal.html')


@app.route('/terminal/threads', methods=['GET'])
def terminal_threads():
    threads = db.session.query(Thread.title).all()
    return jsonify(threads=[t[0] for t in threads])


@app.route('/terminal/cd/<int:id>', methods=['GET'])
def terminal_cd(id):

    return